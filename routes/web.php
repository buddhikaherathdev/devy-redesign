<?php

Auth::routes();

Route::get('/', function () {
    return view('web.pages.home');
});

Route::redirect('/admin',                       '/admin/dashboard');
Route::group([ 'middleware' => [ 'auth' ], 'prefix' => 'admin' ], function () {
    Route::get('/dashboard',                            'Admin\DashboardController@show')->name('dash');
});