<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from bulkitv2.cssninja.io/landing-kits/kit-5/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 22 Aug 2018 04:37:58 GMT -->
<head>
    <!-- Required Meta Tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <title> Bulkit :: Home</title>
    <link rel="icon" type="image/png" href="{{asset('web/images/favicon.png')}}" />

    <!--Core CSS -->
    <link rel="stylesheet" href="{{asset('web/css/icons.min.css')}}">
    <link rel="stylesheet" href="{{asset('web/css/app.css')}}">
    <link rel="stylesheet" href="{{asset('web/css/bulma.css')}}">
    <link rel="stylesheet" href="{{asset('web/css/core_blue.css')}}">
    <link rel="stylesheet" href="{{asset('web/css/icons.min.css')}}">

</head>
<body>

    <div class="pageloader"></div>
    <div class="infraloader is-active"></div>

    @yield('content')

    <div id="backtotop"><a href="#"></a></div>

    <script src="{{asset('web/js/app.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"></script>
    <script src="{{asset('web/js/main.js')}}"></script>
    <script src="{{asset('web/js/landingv4.js')}}"></script>

</body>