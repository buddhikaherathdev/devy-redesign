<div class="hero is-default is-bold is-fullheight">

    <nav class="navbar navbar-wrapper is-cloned">
        <div class="container">

            <div class="navbar-brand">
                <a class="navbar-item" href="/">
                    <h1>DEVY</h1>
                </a>
            </div>

            <div id="is-cloned" class="navbar-menu">
                <div class="navbar-end">
                    <a class="navbar-item is-slide is-centered-tablet" href="/">
                        Pricing
                    </a>
                    <a class="navbar-item is-slide is-centered-tablet" href="/">
                        Login
                    </a>
                    <div class="navbar-item is-button is-centered-tablet">
                        <a id="#signup-btn" href="/" class="button button-cta is-bold btn-align secondary-btn raised">
                            Sign up
                        </a>
                    </div>
                </div>
            </div>

        </div>
    </nav>

    <nav class="navbar navbar-wrapper is-transparent is-static">
        <div class="container">

            <div class="navbar-brand">
                <a class="navbar-item" href="/">
                    <h1>DEVY</h1>
                </a>
            </div>

            <div id="is-static" class="navbar-menu">
                <div class="navbar-end">
                    <a class="navbar-item is-slide is-centered-tablet" href="/">
                        Pricing
                    </a>
                    <a class="navbar-item is-slide is-centered-tablet" href="/">
                        Login
                    </a>
                    <div class="navbar-item is-button is-centered-tablet">
                        <a id="#signup-btn-double" href="/" class="button button-cta is-bold btn-align secondary-btn raised">
                            Sign up
                        </a>
                    </div>
                </div>
            </div>

        </div>
    </nav>


    <!-- Hero content -->
    <div class="hero-body">
        <div class="container">
            <div class="columns is-vcentered">
                <div class="column is-5">
                    <figure class="">
                        {{-- Todo::Make this image dynamic --}}
                        <img src="{{asset('web/images/static/header.jpg')}}" alt="">
                    </figure>
                </div>
                <div class="column is-6 is-offset-1 is-hero-title">
                    <h1 class="title is-1">
                        Unlimited Web Development
                    </h1>
                    <h2 class="subtitle is-4">
                        The technical partner you always wanted
                    </h2>
                    <p class="">
                        <a href="#" class="button button-cta is-bold btn-align secondary-btn raised">
                            Get Started
                        </a>
                    </p>
                </div>
            </div>
        </div>
    </div>

    <!-- Client logos -->
    <div class="hero-foot pt-30 pb-30">
        <div class="container">
            <div class="tabs partner-tabs is-centered bg-overlay">
                <ul>
                    <li><a><img class="partner-logo" src="{{asset('web/images/static/tech_1.png')}}" alt=""></a></li>
                    <li><a><img class="partner-logo" src="{{asset('web/images/static/tech_2.jpg')}}" alt=""></a></li>
                    <li><a><img class="partner-logo" src="{{asset('web/images/static/tech_3.png')}}" alt=""></a></li>
                    <li><a><img class="partner-logo" src="{{asset('web/images/static/tech_4.jpg')}}" alt=""></a></li>
                </ul>
            </div>
        </div>
    </div>
</div>