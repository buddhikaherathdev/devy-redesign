@extends('admin.layouts.main')

@section('content')
<div class="container">
    <div class="card card-login mx-auto mt-5">
        <div class="card-header">Login to DEVY Admin Panel</div>
        <div class="card-body">
            <form method="POST" action="{{ route('login') }}">

                @csrf

                <div class="form-group">
                    <div class="form-label-group">
                        <input type="email" id="inputEmail" name="email"
                               class="form-control{{ $errors->login->has('email') ? ' is-invalid' : '' }}"
                               value="{{ old('email') }}"
                               placeholder="Email address" required="required" autofocus="autofocus">
                        <label for="inputEmail">Email address</label>

                        @if ($errors->has('email'))
                            <span class="invalid-feedback" style="display: block">
                                        <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group">
                    <div class="form-label-group">
                        <input type="password" id="inputPassword" name="password"
                               class="form-control{{ $errors->login->has('password') ? ' is-invalid' : '' }}"
                               placeholder="Password" required="required">
                        <label for="inputPassword">Password</label>

                        @if ($errors->has('password'))
                            <span class="invalid-feedback" style="display: block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                        @endif
                    </div>
                </div>

                <div class="form-group">
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}>
                            Remember Password
                        </label>
                    </div>
                </div>

                <button class="btn btn-primary btn-block" type="submit">Login</button>

            </form>

            <div class="text-center">
                <br />
                <a class="d-block small" href="{{ route('password.request') }}">Forgot Password?</a>
            </div>

        </div>
    </div>
</div>
@endsection


