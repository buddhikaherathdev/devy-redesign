<footer class="sticky-footer">
    <div class="container my-auto">
        <div class="copyright text-center my-auto">
            <span>Copyright © <a href="https://devy.io" target="_blank">DEVY</a></span>
        </div>
    </div>
</footer>