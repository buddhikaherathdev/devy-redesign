<?php

return [

    /***
     * All the user types for the app
     * -----------------------------------------------------------------------------------------------------------------
     */
    'user_types' => [
        'ADMIN' => [
            'id' => 1
        ],
        'USER'  => [
            'id' => 2
        ]
    ]

];