<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Administrator',
            'email' => 'admin@devy.io',
            'user_type' => (int)config('constance.user_types')['ADMIN']['id'],
            'password' => bcrypt('adminpass'),
        ]);
    }
}
