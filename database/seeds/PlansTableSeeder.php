<?php

use Illuminate\Database\Seeder;

class PlansTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('plans')->insert([
            [
            'name' => 'Plan A',
            'value' => 0
            ],
            [
                'name' => 'Plan B',
                'value' => 0
            ],
            [
                'name' => 'Plan C',
                'value' => 0
            ]
        ]);
    }
}
