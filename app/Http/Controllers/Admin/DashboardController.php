<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DashboardController extends Controller
{
    /**
     * DashboardController constructor function
     * -----------------------------------------------------------------------------------------------------------------
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show Admin Panel Dashboard
     * -----------------------------------------------------------------------------------------------------------------
     */
    public function show(){
       return view('admin.pages.dashboard');
    }
}
